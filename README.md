# Pi Burner

A script to watch a server for new file-system images and flash them onto multiple SD cards. Made to run on a Raspberry Pi 2 (probably runs on 3 and 4?), with SD cards plugged into all 4 ports.

## Installation

On a Raspberry Pi:

```
cd /home/pi
git clone https://gitlab.com/bath_open_instrumentation_group/pi_burner
cd pi_burner
# edit pi_burner.service to point at the .zip urls where images are published, then:
sudo su
pip3 install -r requirements.txt
cp pi_burner.service /etc/systemd/system/
systemctl enable pi_burner
systemctl start pi_burner
```
