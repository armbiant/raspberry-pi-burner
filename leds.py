import board
import neopixel

RED = (0, 255, 0)
GREEN = (255, 0, 0)
BLUE = (0, 0, 255)
ORANGE = (80, 220, 0)
PURPLE = (0, 128, 128)


class LEDs:
    def __init__(self, n):
        self.pixels = neopixel.NeoPixel(board.D18, n)

    def __getitem__(self, key):
        return self.pixels[key]

    def __setitem__(self, key, val):
        self.pixels[key] = val
