#!/usr/bin/env python3
from typing import Optional
import os
import pyudev
import re
import requests
import sh
import sys
import time
import shutil

from leds import RED, GREEN, BLUE, ORANGE, PURPLE
import leds

NUMBER_OF_SD_CARDS = 4

LEDs = leds.LEDs(NUMBER_OF_SD_CARDS)

USAGE = """
USAGE: ./{} <image_zip_url>
""".format(
    os.path.basename(__file__)
)

SHA256_CACHE = "image.sha256"
IMG = "image.img"


def main():
    unmount_card()
    if len(sys.argv) != 2:
        print(USAGE)
        sys.exit(1)
    image_zip_url = sys.argv[1]
    print("checking for new images...")
    while 1:
        loop(image_zip_url)


def loop(image_zip_url: str):
    # resolve re-directs
    response = requests.head(image_zip_url, allow_redirects=True)
    image_zip_url = response.url

    sha256_url = image_zip_url + ".sha256"

    current_hash = read_current_hash()
    new_hash = download_hash(sha256_url)
    if new_hash is None:
        print(
            "Could not download sha256 hash, expecting it to be at {}".format(
                sha256_url
            )
        )
    elif new_hash != current_hash:
        for i in range(NUMBER_OF_SD_CARDS):
            LEDs[i] = PURPLE
        valid = download_image(image_zip_url, new_hash)
        zip_file = sha_to_filename(new_hash)
        print("new image downloaded: {}".format(zip_file))
        if valid:
            current_hash = read_current_hash()
            unzip(zip_file)
            for i, card in enumerate(get_all_sd_cards()):
                if card is not None:
                    LEDs[i] = RED
                    write_image_to_sd(current_hash, card)
                    LEDs[i] = GREEN
        else:
            print("Could not validate download, removing {}".format(zip_file))
            sh.rm("-f", zip_file)

    new = get_new_sd_cards(current_hash)
    for i, card in enumerate(get_all_sd_cards()):
        if card is None:
            LEDs[i] = ORANGE
        elif card in new:
            LEDs[i] = PURPLE
        else:
            LEDs[i] = GREEN
    for i, card in enumerate(new):
        if card is not None:
            LEDs[i] = RED
            write_image_to_sd(current_hash, card)
            LEDs[i] = GREEN


def unzip(filename):
    print("unzipping {}".format(filename))
    # unzips to stdout and re-directs it to our desired filename
    # XXX this will not work if there are multiple files in the zip
    # we use os.system instead of subprocess.Popen or sh.unzip because:
    #     - Popen seems to return before it's actually finished
    #     - sh.unzip runs out of memory on a raspberry pi
    os.system("unzip -p {} > {}".format(filename, IMG))
    print("done")


def download_hash(sha256_url: str) -> Optional[str]:
    r = requests.get(sha256_url)
    if r.status_code != 200:
        return None
    return r.text


def read_current_hash() -> Optional[str]:
    try:
        with open(SHA256_CACHE) as f:
            contents = f.read()
    except FileNotFoundError:
        contents = None
    return contents


def sha_to_filename(hash):
    return hash.rstrip().split("  ")[1]


def download_image(image_zip_url, hash):
    print("downloading {}".format(image_zip_url))

    with open("new_image.sha256", "w") as f:
        f.write(hash)
    # get the image zip, '--timestamping' means don't re-download it if it's
    # the the same according to the timestamp and the server response, but
    # overwrite it if it's not
    sh.wget(image_zip_url, "--timestamping")

    try:
        sh.sha256sum("--check", "new_image.sha256")
    except:
        sh.rm("-f", "new_image.sha256")
        return False

    sh.mv("new_image.sha256", SHA256_CACHE)
    return True


def write_image_to_sd(current_hash, card):
    print("copying {} to {}".format(IMG, card.device_node))
    shutil.copyfile(IMG, card.device_node)
    mount_card(card)
    with open("/mnt/.piburner", "w") as f:
        f.write(current_hash)
    unmount_card()
    print("done")


def mount_card(card):
    partition = card.device_node + "1"
    print("mounting {} on /mnt".format(partition))
    sh.mount(partition, "/mnt")
    time.sleep(0.1)


def get_new_sd_cards(current_hash):
    if current_hash is None:
        return (None,) * NUMBER_OF_SD_CARDS
    devices = get_all_sd_cards()
    new = list(devices)
    for i, d in enumerate(devices):
        if d is not None:
            mount_card(d)
            try:
                with open("/mnt/.piburner") as f:
                    contents = f.read()
            except FileNotFoundError:
                contents = None
            unmount_card()
            if contents == current_hash:
                new[i] = None
    return tuple(new)


def unmount_card():
    print("unmounting /mnt")
    try:
        sh.umount("/mnt")
    except sh.ErrorReturnCode_32:
        print('nothing mounted on /mnt')
    time.sleep(0.1)


context = pyudev.Context()


def get_all_sd_cards():
    pattern = re.compile("platform-3f980000_usb-usb-0_1_(\d)_1_0-scsi-0_0_0_0")
    devices = [None] * NUMBER_OF_SD_CARDS
    for d in context.list_devices(subsystem="block", DEVTYPE="disk"):
        if d.get("ID_PATH_TAG") is not None:
            match = pattern.match(d.get("ID_PATH_TAG"))
        if d.get("ID_BUS") == "usb" and match is not None and d.get("ID_PART_TABLE_UUID") is not None:
            try:
                i = int(match.group(1)) - 2
                if i >= NUMBER_OF_SD_CARDS:
                    print("SD CARD NUMBER TOO HIGH!", i)
                    continue
                devices[i] = d
            except IndexError:
                continue
    return tuple(devices)


if __name__ == "__main__":
    main()
